module TestDay exposing (TestDay, fromRaws)

import Array exposing (Array)
import RawTestDay exposing (RawTestDay)
import Parser
import String

type alias TestDay =
    { index : Int
    , date : String
    , day : String
    , total : Float
    , positive : Float
    , percent : Float
    }

fromRaw : Int -> RawTestDay -> Maybe TestDay
fromRaw index raw =
    case String.toFloat raw.total of
        Just total ->
            case String.toFloat raw.positive of
                Just positive -> 
                    Just { index = index
                         , date = String.slice 0 10 raw.date
                         , day = raw.day
                         , total = total
                         , positive = positive
                         , percent = positive / total * 100
                         }
                Nothing -> Nothing
        Nothing -> Nothing

fromRaws : List RawTestDay -> List TestDay
fromRaws raws =
    List.reverse raws
        |> Array.fromList
        |> Array.indexedMap fromRaw
        |> Array.toList
        |> List.filterMap identity
