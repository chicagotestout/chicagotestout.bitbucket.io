module ToolbarView exposing (root)

import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (..)
import Models exposing (Model, Mode(..))
import Msg exposing (..)

root : Model -> Html Msg
root model =
    div [ class "div-toolbar flex-row" ]
        [ label [ class (labelClass model Test) ]
              [ input [ onClick  (SetMode Test)
                      , type_ "radio"
                      ] []
              , text "Testing"
              ]
        , label [ class (labelClass model HospitalizationMode) ]
            [ input [ class ""
                    , onClick (SetMode HospitalizationMode)
                    , type_ "radio"
                    ] []
            , text "Hospitalization"
            ]
        ]

labelClass : Model -> Mode -> String
labelClass model mode =
    if model.mode == mode then
        "flex-1 radio-toolbar radio-toolbar-selected"
    else "flex-1 radio-toolbar"
