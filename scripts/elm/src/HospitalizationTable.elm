module HospitalizationTable exposing (root)

import Html exposing (..)
import Html.Attributes exposing (..)
import Msg exposing (..)
import Hospitalization exposing (Hospitalization)

root : List Hospitalization -> Html Msg
root days =
    div [ class "div-table-wrapper" ]
        [ table_ days ]

table_ : List Hospitalization -> Html Msg
table_ days =
    table [ class "full-width table" ]
        [ thead []
          [ th [] [ text "Date" ]
          , th [] [ text "Ventilator Total Usage" ]
          , th [] [ text "ICU Total Usage" ]
          , th [] [ text "Hospital Bed Total Usage" ]
          ]
        , tbody []
            (days |> List.map row)
        ]

row : Hospitalization -> Html Msg
row day =
    tr []
        [ td [] [ text day.date_string ]
        , td [] [ String.fromFloat day.ventilatorsTotal |> text ]
        , td [] [ String.fromFloat day.icuBedsTotal |> text ]
        , td [] [ String.fromFloat day.acuteNonIcuBedsTotal |> text ]
        ]
         
