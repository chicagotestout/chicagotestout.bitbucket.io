module TestChart exposing (raw)

import Html exposing (..)
import LineChart
import LineChart.Colors as Colors
import LineChart.Junk as Junk
import LineChart.Area as Area
import LineChart.Axis as Axis
import LineChart.Junk as Junk
import LineChart.Dots as Dots
import LineChart.Grid as Grid
import LineChart.Dots as Dots
import LineChart.Line as Line
import LineChart.Colors as Colors
import LineChart.Events as Events
import LineChart.Legends as Legends
import LineChart.Container as Container
import LineChart.Interpolation as Interpolation
import LineChart.Axis.Intersection as Intersection
import Msg exposing (..)
import TestDay exposing (TestDay)

type alias Test =
    { index : Float
    , tests : Float
    }

chart : List TestDay -> Html Msg
chart days =
    LineChart.viewCustom
        { x = Axis.none 1000 .index
        , y = Axis.default 400 "Volume" .tests
        , area = Area.default
        , container = Container.default "test-volume-chart"
        , interpolation = Interpolation.default
        , intersection = Intersection.default
        , legends = Legends.default
        , events = Events.default
        , junk = Junk.default
        , grid = Grid.default
        , line = Line.default
        , dots = Dots.default
        }
        [ LineChart.line Colors.purple Dots.none "total" (total days)
        , LineChart.line Colors.blue Dots.none "positive" (positive days)
        ]


raw : List TestDay -> Html Msg
raw days = List.filter (\d -> d.date /= "Unknown") days
           |> List.reverse
           |> chart

total : List TestDay -> List Test
total days =
    List.map (\d -> Test (toFloat d.index) d.total) days

positive days =
    List.map (\d -> Test (toFloat d.index) d.positive) days
