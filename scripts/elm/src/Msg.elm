module Msg exposing (..)

import Hospitalization exposing (Hospitalization)
import Http
import Json.Encode
import Models exposing (Mode(..))
import RawTestDay exposing (RawTestDay)
import Time

type Msg =  GotRawTestDays (Result Http.Error (List RawTestDay))
    | SetMode Mode
    | UpdateHospitalizationDays Json.Encode.Value
    | UpdateZone Time.Zone
