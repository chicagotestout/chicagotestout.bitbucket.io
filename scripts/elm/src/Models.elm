module Models exposing (..) --todo

import Hospitalization exposing (Hospitalization)
import TestDay exposing (TestDay)
import Time


-- TODO this naming convention (or lack thereof) is a mess.
type alias Model =
    { days : List TestDay
    , hospitalizations : List Hospitalization
    , mode : Mode
    , zone: Time.Zone
    }

type Mode = Test | HospitalizationMode

init : Model
init =
    { days = []
    , hospitalizations = []
    , mode = Test
    , zone = Time.utc
    }

