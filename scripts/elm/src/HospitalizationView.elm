module HospitalizationView exposing (root)

import HospitalizationPercentChart
import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (..)
import Models exposing (Model)
import Msg exposing (..)

import HospitalizationTable

root : Model -> Html Msg
root model = 
    div [ class "div-hospitalization" ]
        [ text ""
        , h2 [] [ text "Chicago Hospitalizations as a Percentage of Capacity" ]
        , p [ class "p" ]
            [ text "This includes both COVID-19 and non-COVID-19 patients." ]
            
        , div [ class "div-chart"]
            [ HospitalizationPercentChart.chart
                  model.zone
                  model.hospitalizations 
            ]
        , HospitalizationTable.root model.hospitalizations
        ]
