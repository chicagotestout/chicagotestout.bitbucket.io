module HospitalizationRawChart exposing (chart)

import Hospitalization exposing (Hospitalization)
import Html exposing (..)
import LineChart
import LineChart.Colors as Colors
import LineChart.Junk as Junk
import LineChart.Area as Area
import LineChart.Axis as Axis
import LineChart.Junk as Junk
import LineChart.Dots as Dots
import LineChart.Grid as Grid
import LineChart.Dots as Dots
import LineChart.Line as Line
import LineChart.Colors as Colors
import LineChart.Events as Events
import LineChart.Legends as Legends
import LineChart.Container as Container
import LineChart.Interpolation as Interpolation
import LineChart.Axis.Intersection as Intersection
import Msg exposing (..)
import Time

type alias ChartModel =
    { date: Float
    , val: Float
    }

chart : Time.Zone -> List Hospitalization -> Html Msg
chart zone days =
    LineChart.viewCustom
        { x = Axis.time zone 1000 "Date" .date
        , y = Axis.default 400 "Volume" .val
        , area = Area.default
        , container = Container.default "hospitalization-raw-chart"
        , interpolation = Interpolation.default
        , intersection = Intersection.default
        , legends = Legends.default
        , events = Events.default
        , junk = Junk.default
        , grid = Grid.default
        , line = Line.default
        , dots = Dots.default
        }
        [ LineChart.line Colors.blue Dots.none "ICU Beds" (icuBeds days)
        , LineChart.line Colors.purple Dots.none "Acute Non-ICU Beds" (nonIcuBeds days)
        , LineChart.line Colors.red Dots.none "Ventilators" (ventilators days)
        ]


icuBeds: List Hospitalization -> List ChartModel
icuBeds days =
    List.map (\d -> (ChartModel  d.date d.icuBedsCovid19)) days

nonIcuBeds: List Hospitalization -> List ChartModel
nonIcuBeds days =
    List.map (\d -> (ChartModel d.date d.acuteNonIcuBedsCovid19)) days

ventilators : List Hospitalization -> List ChartModel
ventilators days =
    List.map (\d -> (ChartModel d.date d.ventilatorsCovid19)) days
