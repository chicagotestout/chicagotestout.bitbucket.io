module TestView exposing (root)

import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (..)
import Models exposing (Model)
import Msg exposing (..)
import TestChart
import TestTable

root : Model -> Html Msg
root model =
    div [ class "div-testing" ]
        [ text ""
        , div [ class "div-chart"]
            [ TestChart.raw model.days ]
        , div [ class "div-table-wrapper" ]
            [ TestTable.root model.days ]
        ]
