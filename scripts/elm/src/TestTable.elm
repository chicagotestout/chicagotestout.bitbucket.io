module TestTable exposing (root)

import Html exposing (..)
import Html.Attributes exposing (..)
import Msg exposing (..)
import TestDay exposing (TestDay)

root : List TestDay -> Html Msg
root days =
    table [ class "full-width table" ]
        [ thead []
          [ th [] [ text "Date" ]
          , th [] [ text "Day" ]
          , th [] [ text "Total Tests" ]
          , th [] [ text "Positive Tests" ]
          , th [] [ text "Percentage" ]
          ]
        , tbody []
            (List.reverse days |> List.map row)
        ]

row : TestDay -> Html Msg
row day =
    tr []
        [ td [] [ text day.date ]
        , td [] [ text day.day ]
        , td [] [ String.fromFloat day.total |> text ]
        , td [] [ String.fromFloat day.positive |> text ]
        , td [] [ String.fromFloat day.percent
                  |> String.slice 0 6
                  |> text
                ]
        ]
         
