port module Ports exposing (..)

import Json.Decode exposing ( Value )

port updateHospitalizations : (Value -> msg) -> Sub msg
