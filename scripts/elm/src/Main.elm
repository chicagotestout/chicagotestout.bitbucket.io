module Main exposing (main)

import Browser
import Hospitalization exposing (Hospitalization)
import HospitalizationView
import Html exposing (..)
import Html.Attributes exposing (..)
import Http
import Json.Decode as Decode exposing (Decoder, list)
import Models exposing (Model, Mode(..))
import Msg exposing (..)
import Ports
import RawTestDay exposing (RawTestDay)
import Task
import TestChart
import TestDay
import TestTable
import TestView
import Time
import ToolbarView

main : Program () Model Msg
main = Browser.element
       { init = init
       , subscriptions = subscriptions
       , update = update
       , view = view
       }

------------------------
init : () -> (Model, Cmd Msg)
init _ = ( Models.init
         , Http.get
               { url = dataPortal
               , expect = Http.expectJson GotRawTestDays rawDaysDecoder
               }
         )

         
subscriptions : Model -> Sub Msg
subscriptions model =
    Sub.batch [ Ports.updateHospitalizations UpdateHospitalizationDays
              ]

update : Msg -> Model -> (Model, Cmd Msg)
update msg model =
    case msg of
        GotRawTestDays result ->
            case result of
                Ok response -> 
                    TestDay.fromRaws response
                        |> \days -> ({model | days = days}
                                    , Task.perform UpdateZone Time.here
                                    )
                Err e  ->
                    case e of
                        Http.BadUrl str ->
                            Debug.log ("BadURL" ++ str)
                              |> \s -> (model, Cmd.none)
                        Http.Timeout ->
                            Debug.log "Timeout"
                              |> \s -> (model, Cmd.none)
                        Http.NetworkError -> (model, Cmd.none)
                        Http.BadStatus code -> String.fromInt code
                                          |> \c -> Debug.log ("BadStatus: " ++ c)
                                          |> \s -> (model, Cmd.none)
                        Http.BadBody str -> Debug.log ("BadBody: " ++ str)
                                            |> \s -> (model, Cmd.none)
        SetMode mode ->
            { model | mode = mode } |> \m -> (m, Cmd.none)

        UpdateHospitalizationDays v ->
            case Decode.decodeValue (Decode.list Hospitalization.decoder) v of
                Ok hospitalizations ->
                    ({model | hospitalizations = hospitalizations }, Cmd.none)
                Err error -> Debug.toString error |> Debug.log
                          |> \str -> (model, Cmd.none)
        UpdateZone zone -> ({ model | zone = zone }, Cmd.none)


view : Model -> Html Msg
view model =
    div [ class "elm-parent" ]
        [ ToolbarView.root model
        , case model.mode of
            Test ->
                TestView.root model
            HospitalizationMode ->
                HospitalizationView.root model
                    
        ]

{---------------------------helpers----------------------------}
        
dataPortal : String
dataPortal = "https://data.cityofchicago.org/resource/t4hh-4ku9.json"

rawDaysDecoder : Decoder (List RawTestDay)
rawDaysDecoder = Decode.list RawTestDay.decoder
