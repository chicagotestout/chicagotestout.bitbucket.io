open ElmES6;
open Js;

type rawHospitalization = {
  date: string,
  ventilators_total_capacity: string,
  ventilators_in_use_total: string,
  ventilators_in_use_covid_19: string,
  icu_beds_total_capacity: string,
  icu_beds_in_use_total: string,
  icu_beds_in_use_covid_19: string,
  acute_non_icu_beds_total_capacity: string,
  acute_non_icu_beds_in_use_total: string,
  acute_non_icu_beds_in_use_covid_19_patients: string,
};

type hospitalization = {
  date_string: string,
  date: float, //posix
  ventilatorsTotalCapacity: int,
  ventilatorsTotal: int,
  ventilatorsCovid19: int,
  icuBedsTotalCapacity: int,
  icuBedsTotal: int,
  icuBedsCovid19: int,
  acuteNonIcuBedsTotalCapacity: int,
  acuteNonIcuBedsTotal: int,
  acuteNonIcuBedsCovid19: int,
};

[@bs.send] external myMap: (list('a), 'a => 'b) => list('b)
  = "map";

[@bs.scope "JSON"] [@bs.val]
external parseRawHospitalizations: string => list(rawHospitalization) =
  "parse";

/* I need to go to confession. */
let toInt: string => int =  [%bs.raw
  {|
  function(str) { return str * 1; }
|}
];

let fromRaw = (raw: rawHospitalization): hospitalization => {
  date_string: String.substring(~from=0, ~to_=10, raw.date), //get rid of time
  date: Date.fromString(raw.date) |> Date.getTime,
  ventilatorsTotalCapacity: raw.ventilators_total_capacity |> toInt,
  ventilatorsTotal: raw.ventilators_in_use_total |> toInt,
  ventilatorsCovid19: raw.ventilators_in_use_covid_19 |> toInt,
  icuBedsTotalCapacity: raw.icu_beds_total_capacity |> toInt,
  icuBedsTotal: raw.icu_beds_in_use_total |> toInt,
  icuBedsCovid19: raw.icu_beds_in_use_covid_19 |> toInt,
  acuteNonIcuBedsTotalCapacity: raw.acute_non_icu_beds_total_capacity |> toInt,
  acuteNonIcuBedsTotal: raw.acute_non_icu_beds_in_use_total |> toInt,
  acuteNonIcuBedsCovid19: raw.acute_non_icu_beds_in_use_total |> toInt,
};

let fromRaws = (raws: list(rawHospitalization)): list(hospitalization) =>
  myMap(raws, raw => fromRaw(raw));

let hospitalizationsEndpoint = "https://data.cityofchicago.org/resource/f3he-c6sv.json";

[@bs.val] [@bs.scope "document"]
external getElementById: string => Dom.element = "getElementById";

module Ports = {
  type t = {
    updateHospitalizations: Elm.sendable(list(hospitalization))
  };
};

let app: Elm.app(Ports.t) =
  Elm.Main.init({ node: getElementById("elm-target") });

let p: Promise.t(unit) = Fetch.fetch(hospitalizationsEndpoint)
  |> Promise.then_(Fetch.Response.text)
  |> Promise.then_(text => parseRawHospitalizations(text) |> Promise.resolve)
  |> Promise.then_(raws => fromRaws(raws) |> Promise.resolve)
//  |> Promise.then_(hosps => Js.log(hosps) |> Promise.resolve)
  |> Promise.then_(hosps => app.ports.updateHospitalizations.send(hosps)
  |> Promise.resolve)
  ;

